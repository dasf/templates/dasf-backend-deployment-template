# Docker setup for the {{ cookiecutter.backend_package_name }} DASF backend module deployment

{% if cookiecutter.project_short_description %}
{{ cookiecutter.project_short_description }}
{% endif %}

This repository contains the files that are necessary to run and build the
images for deploying the {{ cookiecutter.backend_package_name }} project.

{%- if not cookiecutter.project_folder %}

## Installation

You need to have [docker](https://docs.docker.com/get-docker/) installed and
you need to clone this repository. We also recommend that you install
[`docker compose`](https://docs.docker.com/compose/install/).

Once you did this, create a folder named `{{ cookiecutter.project_slug }}` and
clone the `docker` repository:

```bash
# clone the repository
git clone https://{{ cookiecutter.gitlab_host }}/{{ cookiecutter.gitlab_username }}/{{ cookiecutter.project_slug }}/{{ cookiecutter.project_slug }}.git
# change into the cloned repository
cd {{ cookiecutter.project_slug }}
```
Now you need to build and deploy the images. We recommend that you do this via
`docker compose` (see below).

## Using docker compose

The recommended way to build and deploy the files in this repository is to
use `docker compose`. You can install this manually for your operating system
(see the [docker docs](https://docs.docker.com/compose/install/)).

### Building the images

You first need to build the images locally via

```bash
docker compose build
```

This will build two images, the `{{ cookiecutter.project_slug }}_django`
image and the `{{ cookiecutter.project_slug }}_nginx` image.

### Running the containers

To run the containers, simply do a

```bash
docker compose up
```

or if you want to detach it, run

```bash
docker compose up -d
```

## About this repository

### Authors:
{%- for author_info in get_author_infos(cookiecutter.project_authors.split(','), cookiecutter.project_author_emails.split(',')) %}
- [{{ author_info.full_name }}](mailto:{{ author_info.email }})
{% endfor %}


## Technical note

This package has been generated from the template
{{ cookiecutter._template }}.

See the template repository for instructions on how to update the skeleton for
this package.

### Maintainers
{%- for author_info in get_author_infos(cookiecutter.project_maintainers.split(','), cookiecutter.project_maintainer_emails.split(',')) %}
- [{{ author_info.full_name }}](mailto:{{ author_info.email }})
{% endfor %}

## License information

Copyright © {{ cookiecutter.copyright_year }} {{ cookiecutter.copyright_holder }}

{% if cookiecutter.use_reuse == "yes" %}
Code files in this repository is licensed under the {{ cookiecutter.code_license }}.

Documentation files in this repository is licensed under {{ cookiecutter.documentation_license }}.

Supplementary and configuration files in this repository are licensed
under {{ cookiecutter.supplementary_files_license }}.

Please check the header of the individual files for more detailed
information.

{% else %}
All rights reserved.
{% endif %}

{%- endif %}
